//
//  Playlist.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

class Playlist {
    var playlist : [Song]
    var title: String
    var genres: [Song]
    
    init(playlist:[Song], title:String, genres: [Song]){
        self.playlist = playlist
        self.title = title
        self.genres = genres
    }
    
    func addSong(song:Song) {
        playlist.append(song)
    }
    
    func removeSong(song:Song) {
        playlist = playlist.filter() {$0 !== song}
    }
    
    func getTotalDuration() -> Int {
        var total : Int = 0
        for Song in playlist {
            total = Song.duration + total
        }
        return total
    }
    
    func searchGenre(g:Genre) -> [Song]{
        
        for s in playlist{
            if s.genre == g {
                genres.append(s)
            }
        }
        return genres
        
    }
    
}