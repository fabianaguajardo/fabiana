//
//  User.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

class User {
    var userName: String
    var followers: Int
    var following: Int
    
    
    init(userName:String, followers:Int, following:Int){
        self.userName = userName
        self.followers = followers
        self.following = following
    }
    
    func follow(){
        following = following + 1
    }
    
    func unfollow() {
        following = following - 1
    }
    
    func newFollower() {
        followers = followers + 1
    }
    
    func newUnfollower() {
        followers = followers - 1
    }
    
}

let myUser = User(userName: "fabianaguajardo", followers: 1132, following: 879)