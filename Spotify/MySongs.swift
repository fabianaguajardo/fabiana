//
//  MySongs.swift
//  Spotify
//
//  Created by iD Student on 7/9/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import UIKit

var mySongs = [
    Song(title: "Pay No Mind", duration: 234000, artist: "Madeon", genre: Genre.alternative),
    
    Song(title: "Renegades", duration: 193000, artist: "X Ambassadors", genre: Genre.ballad),
    
    Song(title: "Stole the Show", duration: 289000, artist: "Kygo", genre: Genre.pop),
    
    Song(title: "King", duration: 176000, artist: "Years & Years", genre: Genre.pop)
]


class MySongs: UITableViewController {
    /*
// title:String, duration:Int, artist:String, genre:Genre
    var songObjects = mySongs

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songObjects.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("songCell", forIndexPath: indexPath) as! SongCell
        
        let songObject = songObjects[indexPath.row]
        cell.songTitle.text = songObject.title
        
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
*/
}

