//
//  Song.swift
//  Spotify
//
//  Created by iD Student on 7/7/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation


class Song {
    
    var title: String
    var duration: Int
    var artist: String
    var genre: Genre
    
    init(title:String, duration:Int, artist:String, genre:Genre){
        self.title = title
        self.duration = duration
        self.artist = artist
        self.genre = genre
    }
    
    func getDuration() -> String{
        var minutes: Int
        var seconds: Int
        var milliseconds = duration
        
        seconds = milliseconds / 1000
        minutes = seconds/60
        seconds = seconds%60
        
        return "\(minutes)  :  \(seconds)"
    }
    
}

