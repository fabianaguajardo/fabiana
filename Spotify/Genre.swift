//
//  Genre.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

enum Genre {
    case jazz, country, pop, ballad, rock, hiphop, rap, alternative
}